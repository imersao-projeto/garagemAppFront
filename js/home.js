const token = localStorage.getItem("token");
const userid = localStorage.getItem("userid");


// validar se o usuário está logado, senão redirecionar para o login
if (token === null) {
    window.location = "index.html";
}

let ImputUser = document.getElementById("inputUser");
ImputUser.value = "Bem vindo, "+userid;

