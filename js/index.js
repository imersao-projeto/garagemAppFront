//const user = document.getElementById("Inputuser");
//const datafim = document.getElementById("InputDataFim");
//const datainicio = document.getElementById("InputDataInicio");
//const polo = document.getElementById("Polo");


// atribuções dos valores
//Inputuser.value = "Plinitch"
//datainicio.value = "01/08/2018 18:00";
//datafim.value = "01/08/2018 23:59";
//polo.value = "Centro Técnológico";

const userid = document.getElementById("userid");
const senha = document.getElementById("senha");
const btLogin = document.getElementById("btLogin");
//const urlMaster = "http://localhost:8080";
const urlMaster = "http://167.99.101.9/api";
let vuserid = "";
let vsenha = "";

localStorage.clear();

function login(){
    localStorage.clear();
    event.preventDefault();
    vuserid = userid.value;
    vsenha = senha.value;
    //alert("email: "+vemail+"  Senha: "+vsenha);


    let dadosLogin = {
        "userId": vuserid,
        "senha": vsenha
    };
    let url = "/login";

    let resp = post(url,dadosLogin).then(extrairJSON).then(validaResp);
}

function post(endpoint, data){
    return fetch(urlMaster + endpoint,
    {
        method: 'post',
        body: JSON.stringify(data),
        headers: {
            'Authorization': localStorage.getItem('token'), 
            'content-type': 'application/json'
        }
    }).then((response) => {
        if(response.headers.get('Authorization')){
            localStorage.setItem('token', response.headers.get('Authorization').replace("Bearer ", ""));
            localStorage.setItem('userid', vuserid);
        }
        return response;
    });
}


function extrairJSON(resposta){
    return resposta.json();
}

function validaResp(r){
    if(r.mensagem===undefined){
        let endpoint = "home.html";
        window.location = endpoint;
    }else{
        alert(r.mensagem); 
    }  
};




btLogin.onclick = login;