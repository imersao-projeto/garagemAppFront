const token = localStorage.getItem("token");
const userid = localStorage.getItem("userid");
const txtUsuario = document.getElementById("Inputuser");
//const urlBase = "http://localhost:8080";
const urlBase = "http://167.99.101.9/api";
const endpointPolos = "/polos";
const endpointSolicitarToken = "/usuarios/solicitar-token-estacionamento";
const cbPolo = document.getElementById("Polo");
const btGerartoken = document.getElementById("btGerarToken");
const txtDataInicio = document.getElementById("InputDataInicio");
const txtDataFim = document.getElementById("InputDataFim");

// validar se o usuário está logado
if (token === null) {
    window.location = "index.html";
}

let ImputUser = document.getElementById("inputUser");
ImputUser.value = "Bem vindo, "+userid;

function extrairJSON(resposta){
    return resposta.json();
}

function tratarErro(resposta) {
    alert('deu erro man');
}

function sucessoToken(json) {
    alert('sucesso')
}

function gerarToken() {
    let data = {
        "polo": {
            "descricao": cbPolo.options[cbPolo.selectedIndex].value
        },
        "dataHoraInicio": txtDataInicio.value,
        "dataHoraFim": txtDataFim.value
    }    
    console.log(data);
    fetch(urlBase + endpointSolicitarToken,
        {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Authorization': token, 
                'content-type': 'application/json'
            }
        }).then((response) => {
            if (!response.ok) {
                response.json().then(function(object) {
                    if (object.hasOwnProperty("mensagem")) {
                        alert(object.mensagem);
                    } else {
                        alert("Erro não esperado.");
                    }
                });
            } else {
                response.json().then(function(object) {
                    window.location = "token.html";
                })
            }
        });
}

// Popular objetos
txtUsuario.value = userid;
fetch(urlBase + endpointPolos,
    {
        method: 'get',
        headers: {
            'Authorization': token, 
            'content-type': 'application/json'
        }
    }).then(extrairJSON).then((polos) => {        
        polos.forEach(function(polo) {                    
            let el = document.createElement("option");
            el.textContent = polo.descricao;
            el.value = polo.descricao;
            cbPolo.appendChild(el);        
        });
    });

btGerartoken.onclick = gerarToken;