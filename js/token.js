const token = localStorage.getItem("token");
const userid = localStorage.getItem("userid");
const txtUserId = document.getElementById("Inputuser");
//const urlBase = "http://localhost:8080";
const urlBase = "http://167.99.101.9/api";
const endpointTokens = "/usuarios/tokens";
const endpointUsarToken = "/usuarios/validar-token-estacionamento";
const txtToken = document.getElementById("Inputtoken");
const qrToken = document.getElementById("qrToken");
const btUsarToken = document.getElementById("btToken");
let tokenString = "";
const txtStatusToken = document.getElementById("InputStatusToken");
const txtDataInicio = document.getElementById("InputDataInicio");
const txtDataFim = document.getElementById("InputDataFim");

// validar se o usuário está logado
if (token === null) {
    window.location = "index.html";
}
console.log(token);

let ImputUser = document.getElementById("inputUser");
ImputUser.value = "Bem vindo, "+userid;

txtUserId.value = userid;
btUsarToken.disabled = true;

fetch(urlBase + endpointTokens,
    {
        method: 'get',
        headers: {
            'Authorization': token, 
            'content-type': 'application/json'
        }
    }).then((response) => {
        if (!response.ok) {
            response.json().then(function(object) {
                if (object.hasOwnProperty("mensagem")) {
                    alert(object.mensagem);
                    btUsarToken.disabled = true;
                } else {
                    alert("Erro não esperado.");
                    btUsarToken.disabled = true;
                }
            });
        } else {
            response.json().then(function(object) {
                tokenString = object.token;
                //
                txtToken.value = object.token;
                if (object.status === true) {
                    txtStatusToken.value = "Disponível";
                    btUsarToken.disabled = false;
                } else {
                    txtStatusToken.value = "Indisponível";
                    btUsarToken.disabled = true;
                }

                txtDataInicio.value = object.dataHoraInicial;
                txtDataFim.value = object.dataHoraFinal;

                new QRCode(qrToken, object.token);
            })
        }
    });

function usarToken() {
    let data = {
        "token": tokenString
    }

    fetch(urlBase + endpointUsarToken,
        {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Authorization': token, 
                'content-type': 'application/json'
            }
        }).then((response) => {
            if (!response.ok) {
                response.json().then(function(object) {
                    if (object.hasOwnProperty("mensagem")) {
                        alert(object.mensagem);
                        window.location = "token.html";
                    } else {
                        alert("Erro não esperado.");
                    }
                });
            } else {
                window.location = "token.html";
            }
        });
}

btUsarToken.onclick = usarToken;